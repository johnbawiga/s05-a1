<%@ page language="java" %>
<%@ page import="java.util.*,java.text.*" %>
<html>
<head>
  <title>ACTIVITY</title>
</head>
<body>
  <h1>Our Date and Time now is...</h1>
  
  <% System.out.println("Hello from EJ"); %>
  <% 
    TimeZone manilaTimezone = TimeZone.getTimeZone("Asia/Manila");
    TimeZone japanTimezone = TimeZone.getTimeZone("Asia/Tokyo");
    TimeZone germanyTimezone = TimeZone.getTimeZone("Europe/Berlin");

    SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss ");

    dateFormatter.setTimeZone(manilaTimezone);
    dateFormatter.setTimeZone(japanTimezone);
    dateFormatter.setTimeZone(germanyTimezone);

    out.println("<ul> <li>Manila: " + dateFormatter.format(new Date()) +
    			"</li><li> Japan: " + dateFormatter.format(new Date()) + 
    			"</li><li> Germany: " + dateFormatter.format(new Date()) + "</li></ul>");

    %>
    <%!
		private int initVar = 0;
		private int serviceVar = 0;
		private int destroyVar = 0;
	%>
	<!-- JSP method declaration -->
	<%!
		public void jspInit(){
			initVar++;
			System.out.println("jspInit()"+initVar);
		}
		public void jspDestroy(){
			destroyVar++;
			System.out.println("jspDestroy(): destroy"+destroyVar);
		}
	%>
    <%
		serviceVar++;
		String content1 = "content1: " + initVar;
		String content2 = "content2: " + serviceVar;
		String content3 = "content3: " + destroyVar;
	%>
	<h1>JSP</h1>
	<p><%= content1%></p>
	<p><%= content2%></p>
	<p><%= content3%></p>
</body>
</html>